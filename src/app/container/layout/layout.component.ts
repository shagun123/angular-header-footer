import {Component} from '@angular/core';

@Component({
  selector: 'youtube-layout',
  template: `
    <div style="min-height:100vh" fxLayout="column"
         fxLayoutAlign="stretch">
      <youtube-header></youtube-header>
      <router-outlet (activate)="this.onActivate()"></router-outlet>
      <span fxFlex="1 1 auto"></span>
      <youtube-footer></youtube-footer>
    </div>
  `,
  styles: [``]
})

export class LayoutComponent {

  constructor() {
  }

  onActivate() {
    window.scroll(0, 0);
  }
}
