import {Component} from '@angular/core';

@Component({
  selector: 'youtube-header',
  template: `
    <mat-toolbar color="primary" style="padding: 5px; height: 80px">
      <button color="secondry" [routerLinkActiveOptions]="{exact:true}"
              routerLink="" routerLinkActive="selected"
              mat-button>Home
      </button>
      <button color="secondry" mat-button routerLink="/videos" routerLinkActive="selected">Videos</button>
    </mat-toolbar>
  `,
  styles: [`
    .selected {
      background-color: azure;
    }

    button {
      color: black;
    }
  `]
})

export class HeaderComponent {

  constructor() {
  }
}
