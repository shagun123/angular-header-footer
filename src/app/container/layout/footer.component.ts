import {Component} from '@angular/core';

@Component({
  selector: 'youtube-footer',
  template: `
    <div fxLayout="row" fxFlex="1 1 auto" style="background-color: black;
     height: 50px;"
         fxFlexAlign="space-between center">
      <h1>Your solution to all software</h1>
      <div fxLayout="row">
        <p>Privacy</p>
        <p>Terms and Condition</p>
        <p>FAQ</p>
      </div>
    </div>
  `,
  styles: [`
    h1, p {
      color: white
    }
  `]
})

export class FooterComponent {

  constructor() {
  }
}
