import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './container/dashboard/dashboard.component';
import {HomeComponent} from './container/dashboard/home.component';
import {VideosComponent} from './container/dashboard/videos.component';


const routes: Routes = [
  {
    path: '', component: DashboardComponent,
    children: [{path: '', component: HomeComponent},
      {path: 'videos', component: VideosComponent}]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
