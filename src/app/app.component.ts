import {Component} from '@angular/core';

@Component({
  selector: 'youtube-root',
  template: `
    <router-outlet></router-outlet>
  `,
  styles: [``]
})

export class AppComponent {

  constructor() {
  }
}
