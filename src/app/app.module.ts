import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule, FlexModule} from '@angular/flex-layout';
import {MaterialModule} from './material.module';
import {HomeComponent} from './container/dashboard/home.component';
import {VideosComponent} from './container/dashboard/videos.component';
import {HeaderComponent} from './container/layout/header.component';
import {FooterComponent} from './container/layout/footer.component';
import {DashboardComponent} from './container/dashboard/dashboard.component';
import {LayoutComponent} from './container/layout/layout.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    VideosComponent,
    HeaderComponent,
    FooterComponent,
    DashboardComponent,
    LayoutComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    FlexModule,
    MaterialModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
